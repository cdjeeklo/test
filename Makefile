SOURCE=src/
CLIENT=public/
MODULE=modules/
MD_FILES=$(wildcard $(SOURCE)*.md)
GEVORDERD=pong dodgeball
EXPERT=simon galgje memory klok

GRAAD=starter
$(patsubst %, $(CLIENT)%/*, $(GEVORDERD)) : GRAAD=gevorderd
$(patsubst %, $(CLIENT)%/*, $(EXPERT)) : GRAAD=expert

.PHONY: copylibs

all: copylibs $(patsubst $(SOURCE)%.md, $(CLIENT)%.html, $(MD_FILES))

# TODO: each presentation must have it's own id (and secret for the master)
$(CLIENT)%.html: $(SOURCE)%.md $(SOURCE)head.html $(SOURCE)tail.html
	$(info $@)
	@mkdir -p $(dir $@)
	@rm -f $@
	@cat $(SOURCE)head.html $< $(SOURCE)tail.html >> $@
	@sed -i 's/<body>/<body class="${GRAAD}">/' $@
	@cp $@ $(dir $@)master_$(notdir $@)
	@sed -i 's/client\.js/master.js/' $(dir $@)master_$(notdir $@)

copylibs:
	@cp $(MODULE)reveal.js/dist/reveal* $(CLIENT)lib/
	@cp -r $(MODULE)reveal.js/plugin/*  $(CLIENT)lib/plugins
	@cp $(SOURCE)client.js $(CLIENT)client.js
	@cp $(SOURCE)master.js $(CLIENT)master.js
