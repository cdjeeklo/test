# CoderDojo Eeklo Scratch

[![pipeline status](https://gitlab.com/cdjeeklo/test/badges/main/pipeline.svg)](https://gitlab.com/cdjeeklo/test/-/commits/main)

Gitlab's continuous integration tool will build this project, and update the pages served at https://cdjeeklo.gitlab.io/test/ when it receives changes to the "main" branch.

~~The goal is to run the master presentation locally on your computer with `python localServe.py`. This will overwrite `index.js` with `src/master.js`.~~

**edit**: CORS problems anyway, trying to publish the master with a `master_<FILE>.html` name and swapping `index.js` with either `client.js` or `master.js`.

todo:

* scratch export processing: a shell script to more easily get the code block syntax and requisites
* service worker for offline caching
* use our own updated socket.io server, [broadcast with a namespace](https://github.com/reveal/multiplex/issues/8), [use hmac256 for socket.io signature](https://github.com/reveal/multiplex/issues/6)
