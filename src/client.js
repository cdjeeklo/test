// import Reveal      from './lib/reveal.esm.js';
// import Markdown    from './lib/plugins/markdown/markdown.esm.js';
Reveal.initialize({
  controls: false,
  keyboard: false,
  touch: false,
  loop: false,
  autoAnimate: true,
  transition: 'fade',
  hash: true,
  fragmentInURL: false,
  history: false,
  disableLayout: false,
  navigationMode: 'linear',
  preloadIframes: true,
  multiplex: {
    secret: null,
    id: '6070024114d3e35a',
    url: 'https://reveal-multiplex.glitch.me/'
  },
  dependencies: [
    { src: 'https://reveal-multiplex.glitch.me/socket.io/socket.io.js', async: true },
    { src: 'https://reveal-multiplex.glitch.me/client.js', async: true },
  ],
  plugins: [ RevealMarkdown ]
});
