// import Reveal      from './lib/reveal.esm.js';
// import Markdown    from './lib/plugins/markdown/markdown.esm.js';
// import Notes       from './lib/plugins/notes/notes.esm.js';
Reveal.initialize({
  autoSlide: 3000,
  loop: false,
  autoAnimate: true,
  transition: 'fade',
  hash: true,
  fragmentInURL: false,
  history: false,
  disableLayout: false,
  navigationMode: 'linear',
  preloadIframes: true,
  multiplex: {
    secret: '16429649132016278',
    id: '6070024114d3e35a',
    url: 'https://reveal-multiplex.glitch.me/'
  },
  dependencies: [
    { src: 'https://reveal-multiplex.glitch.me/socket.io/socket.io.js', async: true },
    { src: 'https://reveal-multiplex.glitch.me/master.js', async: true },
    { src: 'https://unpkg.com/reveal-notes-server/client.js', async: true }
  ],
  plugins: [ RevealNotes, RevealMarkdown ]
});

// //https://reveal-multiplex.glitch.me/master.js
// // Don't emit events from inside of notes windows
// if ( window.location.search.match( /receiver/gi ) ) { return; }
// const multiplex = Reveal.getConfig().multiplex;
// const socket = io.connect( multiplex.url );
// function post() {
//   const messageData = {
//     state: Reveal.getState(),
//     secret: multiplex.secret,
//     socketId: multiplex.id
//   };
//   socket.emit( 'multiplex-statechanged', messageData );
// };
// // post once the page is loaded, so the client follows also on "open URL".
// window.addEventListener( 'load', post );
// // Monitor events that trigger a change in state
// Reveal.on( 'slidechanged', post );
// Reveal.on( 'fragmentshown', post );
// Reveal.on( 'fragmenthidden', post );
// Reveal.on( 'overviewhidden', post );
// Reveal.on( 'overviewshown', post );
// Reveal.on( 'paused', post );
// Reveal.on( 'resumed', post );
