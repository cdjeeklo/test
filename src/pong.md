

# Pong

![Het pong spel heeft een rode vloer, het pallet (de plank) is blauw, de achtergrond is wit
en in de bovenhoeken is het geel. Links onderaan is de score variabele weergegeven met de waarde 19.](img/pong.png)

&copy; [frank](frank.neven@uhasselt.be), [wim](wim.lamotte@uhasselt.be), [jonny](jonny.daenen@uhasselt.be), [bart](de.roy.bart@gmail.com)

---

# Pong

<http://scratch.mit.edu/projects/48528496/>

<http://bit.ly/uhscratchstudio>

Note:
De beste manier om met Pong vertrouwd te raken is het eerst zelf te spelen!

---

# Pong

* [x] x- en y-coördinaten, richting van een sprite
* [x] genereren van willekeurige getallen             <!-- .element: class="fragment" -->
* [x] variabelen, condities, herhaling, als-dan-test  <!-- .element: class="fragment" -->
* [x] klonen, signalen, tijdklok, eigen blokken       <!-- .element: class="fragment" -->

Note:
In dit project maak je kennis met de volgende concepten: coördinaten, richting, willekeurige getallen, variabelen, condities, herhaling, als-dan-test, klonen, signalen, tijdklok en eigen blokken.
Elke stap bestaat uit een opdracht en uitleg over nieuwe blokken.

---

# Plank

## Opdracht:

1. teken een "plank" sprite
2. laat de plank horizontaal de muisaanwijzer volgen    <!-- .element: class="fragment" -->

---

# Plank

![Penseel knop om een nieuwe sprite te tekenen](img/pong-1.1.png)
![Rechthoek gereedschap](img/pong-1.2.png)                        <!-- .element: class="fragment" -->
![Verfpot gereedschap om met kleur te vullen](img/pong-1.3.png)   <!-- .element: class="fragment" -->
![Knop met label middelpunt van uiterlijk](img/pong-1.4.png)      <!-- .element: class="fragment" -->

Note:
zet het middelpunt in het midden van de rechthoek

---

# Plank

![De blauwe rechthoek staat centraal op de oorsprong van het assenstelsel](img/pong-1.4.1.png)

Note:
De blauwe rechthoek staat centraal op de oorsprong van het assenstelsel

---

## X en Y coördinaten

![De huidige x en y coördinaat van de muis worden in het venster getoond.](img/pong-1.5.png)

Note:
Beweeg met je muis over de achtergrond en bekijk hoe de coördinaten veranderen.
De huidige x en y coördinaat van de muis worden in het venster getoond.

---

Een **opdracht** blokje voert een opdracht uit.

<code class="b l">ga naar x: (0) y: (-120)</code>

<code class="b l fragment">maak x (10)</code>

Note:
verplaats de sprite naar positie `(0, -120)`; maak x-coördinaat gelijk aan `10`

---

Een **functie** blokje geeft een waarde terug.

<code class="b l">(muis x)</code>

Note:
Dit is de x-coördinaat van de huidige muis positie

---

Een **controle blok** beïnvloed de uitvoering van het programma.

<code class="b s">wanneer groene vlag wordt aangeklikt</code>

<code class="b m fragment">herhaal</code>

Note:
Alle blokken onder dit blokje worden uitgevoerd wanneer op de groene vlag wordt geklikt.
Alle blokken in dit blok worden herhaald tot het programma gestopt wordt.

---

## Opdracht:

wanneer op de groene vlag geklikt wordt, moet de plank horizontaal bewegen en de muis volgen

---

## Blokken:

<code class="b">wanneer groene vlag wordt aangeklikt</code>
<code class="b fragment">herhaal</code>
<code class="b fragment">ga naar x: (0) y: (-120)</code>
<code class="b fragment">maak x (10)</code>
<code class="b fragment">(muis x)</code>

Test je programma!
<!-- .element: class="testen fragment" -->

---

```scratch
wanneer groene vlag wordt aangeklikt
ga naar x: (0) y: (-120)
herhaal
  maak x (muis x)
```

---

# Bal

## Opdracht:

1. kies een bal uit de bibliotheek
2. laat de bal naar beneden vallen en aan de rand botsen  <!-- .element: class="fragment" -->
3. zorg dat de bal niet altijd in dezelfde richting valt  <!-- .element: class="fragment" -->

Note:
(we negeren voorlopig de plank)

---

## Blokken:

<code class="b">wanneer groene vlag wordt aangeklikt</code>
<code class="b fragment">herhaal</code>
<code class="b fragment">maak grootte (50) %</code>
<code class="b fragment">(willekeurig getal tussen (135) en (255))</code>
<code class="b fragment">ga naar x: (0) y: (130)</code>
<code class="b fragment">richt naar (10 v) graden</code>
<code class="b fragment">neem (10) stappen</code>
<code class="b fragment">keer om aan de rand</code>

Test je programma!
<!-- .element: class="testen fragment" -->

Note:
* richt naar (10 v) graden: richt de sprite naar een bepaalde richting
* neem (10) stappen: zet een aantal stappen in deze richting hoe groter dit getal, hoe verder de sprite stapt
* maak grootte (50) %: verander de grootte van een sprite
* keer om aan de rand: verander de richting als de sprite de rand raakt
* (willekeurig getal tussen (135) en (255)): geeft een willekeurig getal tussen twee grenzen

---

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (-120)
richt naar (willekeurig getal tussen (135) en (255)) graden
herhaal
  neem (10) stappen
  keer om aan de rand
```

---

# Kaatsen

## Opdracht:

1. maak een rode lijn onderaan de achtergrond
2. stop het spel wanneer de bal de rode lijn raakt  <!-- .element: class="fragment" -->
3. laat de bal terugkaatsen als die de plank raakt  <!-- .element: class="fragment" -->

Note:
de richting waarin de bal kaatst mag willekeurig zijn

---

# Kaatsen

## Nieuwe blokken:

<code class="b">stop [alle v]</code>
<code class="b fragment">als <> dan</code>
<code class="b fragment">&lt;raak ik kleur [#aa7733] ?&gt;</code>

Test je programma!
<!-- .element: class="testen fragment" -->

Note:
* stopt alle sprites
* **controle**: als de conditie waar is dan worden alle blokken in dit blok uitgevoerd
* **conditie** is waar of onwaar: indien de sprite een bepaalde kleur raakt

---

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (-120)
richt naar (willekeurig getal tussen (135) en (255)) graden
herhaal
  neem (10) stappen
  keer om aan de rand
  als &lt;raak ik kleur [#4400ff] ?&gt; dan
    richt naar (willekeurig getal tussen (-45) en (45)) graden
  einde
  als &lt;raak ik kleur [#ff0000] ?&gt; dan
    stop [alle v]
```

---

# Score

## Opdracht:

1. houd een score bij
2. telkens de bal de plank raakt, wordt de score met één verhoogd  <!-- .element: class="fragment" -->

---

# Score

## Nieuwe blokken:

<code class="b">maak [score v] (0)</code>
<code class="b fragment">(score)</code>
<code class="b fragment">verander [score v] met (1)</code>

Note:
Maak een nieuw variable aan en noem deze *score*.
Vink aan om op canvas weer te geven.
Met **data blokken** kan je data manipuleren en opslaan.
* zet de waarde van score op nul
* verandert de waarde van score (negatief gaat ook!)
* geeft de huidige waarde van score

---

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (-120)
richt naar (willekeurig getal tussen (135) en (255)) graden
maak [score v] (0)
herhaal
  neem (10) stappen
  keer om aan de rand
  als &lt;raak ik kleur [#4400ff] ?&gt; dan
    richt naar (willekeurig getal tussen (-45) en (45)) graden
    verander [score v] met (1)
  einde
  als &lt;raak ik kleur [#ff0000] ?&gt; dan
    stop [alle v]
```

---

# Tweede bal

## Opdracht:

1. maak twee gele bovenhoeken
2. als de bal een hoek raakt verschijnt er een tweede bal
3. nooit meer dan twee ballen

Note:
1. maak de twee bovenhoeken van de achtergrond geel
2. wanneer de bal een hoek raakt, verschijnt er een tweede bal,
   deze bal mag de rode lijn niet raken
3. let op: er mogen nooit meer dan twee ballen in het spel zijn
   (tip: gebruik een variabele om bij te houden hoeveel ballen er zijn)

---

# Tweede bal

## Nieuwe blokken:

<code class="b">wanneer ik als kloon start</code>
<code class="b fragment">maak een kloon van [mijzelf v]</code>
<code class="b fragment"><> en <></code>
<code class="b fragment">stuiter :: custom</code>
<code class="b fragment"><[] = []></code>

---

# Tweede bal

## Klonen

<code class="b l">maak een kloon van [mijzelf v]</code>
<code class="b s fragment">wanneer ik als kloon start</code>

Note:
* maakt een kloon (kopie) van deze sprite
* de blokken onder dit blok worden uitgevoerd telkens er een kloon gemaakt wordt

---

## Eigen blokje

```scratch
definieer (stuiter :: stack) :: custom hat
neem (10) stappen
keer om aan de rand
als &lt;raak ik kleur [#4400ff] ?&gt; dan
  richt naar (willekeurig getal tussen (-45) en (45)) graden
  verander [score v] met (1)
einde
als &lt;raak ik kleur [#ff0000] ?&gt; dan
  stop [alle v]
```

Note:
Eigen blokken houden programma's overzichtelijk.

---

Condities controleren of iets waar is.

<code class="b l"><[] = []></code> <code class="b l fragment"><(aantalBallen) = [1]></code>

<code class="b l fragment"><> en <></code> <code class="b l fragment"><< raak ik kleur [#00ff00] ?> en <(aantalBallen) = [1]> :: green ></code>

Note:
dit blok wordt waar als beide waarden gelijk zijn
(voorbeeld) is waar indien de waarde van aantalBallen gelijk is aan 1
dit blok wordt waar als beide condities waar zijn
(voorbeeld) is waar indien de sprite de kleur geel raakt **en** de waarde van aantalBallen gelijk is aan 1

---

## Gebruik je blokje

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
maak [aantalBallen] [1]
maak [score v] [0]
herhaal
  als << raak ik kleur [#4400ff] ?> en <(aantalBallen) = [1]>> dan
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
```
<!-- .element: data-id="code-animation" -->

---

## Gebruik je blokje

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
maak [aantalBallen] [1]
maak [score v] [0]
herhaal
  stuiter :: custom
  als << raak ik kleur [#4400ff] ?> en <(aantalBallen) = [1]>> dan
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
```
<!-- .element: data-id="code-animation" -->

---

## Gebruik je blokje

```scratch
wanneer ik als kloon start
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
herhaal
  stuiter
```

---

# Twee ballen modus

1. zorg ervoor dat de tweede bal na 5 seconden verdwijnt
2. telkens er twee ballen zijn, wordt de plank langer        <!-- .element: class="fragment" -->
3. verdwijnt de tweede bal, dan wordt de plank terug korter  <!-- .element: class="fragment" -->

---

## Nieuwe blokken:

<code class="b"><(klok) > [5] ></code>
<code class="b fragment">zet klok op 0</code>
<code class="b fragment">verander uiterlijk naar [plank-groot v]</code>
<code class="b fragment">zend signaal [plank groot v]</code>
<code class="b fragment">wanneer ik signaal [plank groot v] ontvang</code>
<code class="b fragment">verwijder deze kloon</code>

Note:
* zet de tijdklok op nul
* conditie wordt waar indien de waarde van de tijdklok groter is dan vijf
* verandert uiterlijk van de sprite
* stuurt het signaal *plank groot*
* wanneer deze sprite het signaal *plank groot* ontvangt dan worden de blokken onder dit blok uitgevoerd

---

**plank**
<!-- .element: class="right" -->

```scratch
wanneer groene vlag wordt aangeklikt
ga naar x: (0) y: (-120)
verander uiterlijk naar [plank-klein v]
herhaal
  maak x (muis x)
```


```scratch
wanneer ik signaal [plank groot v] ontvang
verander uiterlijk naar [plank-groot v]

wanneer ik signaal [plank klein v] ontvang
verander uiterlijk naar [plank-klein v]
```
<!-- .element: class="right" -->

---

**bal**
<!-- .element: class="right" -->

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
maak [aantalBallen] [1]
maak [score v] [0]
herhaal
  stuiter :: custom
  als << raak ik kleur [#4400ff] ?> en <(aantalBallen) = [1]>> dan
    zet klok op 0
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
    zend signaal [plank groot v]
  einde
einde
```

---

**bal**
<!-- .element: class="right" -->

```scratch
wanneer ik als kloon start
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
herhaal
  stuiter :: custom
  als <(klok) > [5]> dan
    maak [aantalBallen v][1]
    zend signaal [plank klein v]
    verwijder deze kloon
```

---

# EINDE!


