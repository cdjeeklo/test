
# test
<!-- .element: class="r-fit-text" -->

---

# test

```scratch
wanneer groene vlag wordt aangeklikt
```
<!-- .element: data-id="code-animation" -->

---

# test

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
```
<!-- .element: data-id="code-animation" -->

---

# test

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
herhaal
einde
```
<!-- .element: data-id="code-animation" -->

---

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
herhaal
  als <> dan
    zet klok op 0
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
    zend signaal [plank groot v]
  einde
einde
```
<!-- .element: data-id="code-animation" -->

---

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
herhaal
  als << raak ik kleur [#4400ff] ?> en <(aantalBallen) = [1]>> dan
    zet klok op 0
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
    zend signaal [plank groot v]
  einde
einde
```
<!-- .element: data-id="code-animation" -->

